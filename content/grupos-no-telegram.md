---
title: "Grupos no Telegram"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# Grupos no Telegram

A comunidade brasileira mantem alguns grupos sobre Debian no telegram. Cada
grupo tem suas peculariedades, regras, formas de administrar, etc.

## Debian Brasil

- [@debianbrasil](https://t.me/debianbrasil)
- Descrição: O foco desse grupo é DEBIAN. Sobre outras distros e GNU/Linux em
geral, procure outros grupos.

## Debian Br:

- [@debianbr](https://t.me/debianbr)
- Descrição: Grupo criado para conversarmos sobre Debian, e outros temas!

## Debian notícias em português

- [@DebianBrasilNoticias](https://t.me/DebianBrasilNoticias)

## Debian Educacional Brasil

- [@DebianEduBR](https://t.me/DebianEduBR)

## Debian equipe tradução para o português

- [@debl10nptBR](https://t.me/debl10nptBR)

## Debian Art & Design

- [@debiandesign](https://t.me/debiandesign)