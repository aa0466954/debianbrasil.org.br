---
title: "Opencast sobre Debian"
kind: article
created_at: 2013-09-30 09:11
author: Helio Loureiro
---

Participei, junto com o Guaraldo, de um opencast pra falar um pouco sobre o Debian.

Foi mais direcionado às pessoas que usam Ubuntu mas que gostariam de conhecer
as origens desse pelo Debian.

<https://youtu.be/K6-oNg_I3wY>