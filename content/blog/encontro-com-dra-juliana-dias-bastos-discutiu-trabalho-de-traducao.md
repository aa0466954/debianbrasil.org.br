---
title: "Encontro com Dra Juliana Dias Bastos discutiu trabalho de tradução"
kind: article
created_at: 2021-05-18 20:00
author: Paulo Henrique de Lima Santana
---

A [equipe de localização](https://wiki.debian.org/Brasil/Traduzir) do Debian
para português do Brasil (l10n.debian.org.br) realizou um encontro virtual no
dia 10 de maio para discutir sobre o trabalho de tradução que vem desenvolvendo.

As temáticas enfatizadas foram a diversidade (o português do Brasil possui dois
gêneros gramaticais geralmente bem demarcados) e a equivalência de termos
técnicos entre português e outros idiomas.

A doutora em Letras/Tradução Juliana Dias Bastos foi convidada para fazer uma
breve apresentação a partir da perspectiva acadêmica e para participar dos
debates.

Tivemos também a grata presença da equipe de tradução do LibreOffice, o que
muito enriqueceu nossas reflexões. Obrigado(a) a todos(as) que participaram!