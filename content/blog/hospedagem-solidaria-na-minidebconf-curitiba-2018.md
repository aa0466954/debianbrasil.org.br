---
title: "Hospedagem Solidária na MiniDebConf Curitiba 2018"
kind: article
created_at: 2018-02-09 17:21
author: Paulo Henrique de Lima Santana
---

A Comunidade Debian quer dar uma ajuda para quem precisa de um cantinho para
ficar durante [MiniDebConf Curitiba 2018](https://minidebconf.curitiba.br/2018/) que
acontecerá de 11 a 14 de abril.

Assim, criamos 2 tópicos no fórum para que você possa oferecer ou pedir um
quarto (cama, sofá, etc) durante a MiniDebConf.

Não há coisa melhor que aproveitar um evento próximo à pessoas que compartilham
dos mesmo valores!

O fórum foi desativado.

![Banner casa](/blog/imagens/banner-casa.png =400x)