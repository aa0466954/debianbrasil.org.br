---
title: "Fotos da MiniDebConf Curitiba 2016"
kind: article
created_at: 2016-03-11 23:37
author: Paulo Henrique de Lima Santana
---

Estamos reunindo aqui links com as fotos da
[MiniDebConf Curitiba 2016](http://br2016.mini.debconf.org/).

**Álbum da Comunidade Curitiba:**

<https://www.flickr.com/photos/curitibalivre/albums/72157665695548695>

**Álbum do João Eriberto:**

[https://www.flickr.com/photos/140964525@N07/albums/72157665735147025](https://www.flickr.com/photos/140964525@N07/albums/72157665735147025)

**Álbum Giovani Ferreira:**

[https://www.flickr.com/photos/132049874@N03/sets/72157665750067495](https://www.flickr.com/photos/132049874@N03/sets/72157665750067495/)

Se você tiver fotos do evento, pode enviar para o email abaixo pra gente incluir aqui nesta página:

contato@br2016.mini.debconf.org